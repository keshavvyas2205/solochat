package com.example.solochat.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Users extends IbSupport {
	@Id
	@GeneratedValue(generator = "user_id_generator")
	@SequenceGenerator(
			name = "user_id_generator",
			sequenceName = "user_id_generator",
			initialValue = 1
			)
	private long id;
	private String userName;
}
