package com.example.solochat.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.solochat.model.ChatMessage;
import com.example.solochat.model.Users;

public interface ChatMessageRepository extends JpaRepository<ChatMessage, Long> {

//	Page<ChatMessage> findByFromUserAndToUserOrderByCreatedAt(Users users, Users users2,Pageable pageable);

	//List<ChatMessage> findByFromUserAndToUserOrderByCreatedAt(Users users, Users users2);

	List<ChatMessage> findByFromUserAndToUserOrderByCreatedAt(Users users, Users users2,Pageable pageable);

}
