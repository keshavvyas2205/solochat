package com.example.solochat.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer  {
	public void registerStompEndpoints(final StompEndpointRegistry registery)
	{
		registery.addEndpoint("/websocket").setAllowedOrigins("*");
	}
	 
	public void configureMessageBroker(final MessageBrokerRegistry registery)
	{
		registery.setApplicationDestinationPrefixes("/app");
		registery.enableSimpleBroker("/topic","/users");
	}
}
