package com.example.solochat.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;

import com.example.solochat.dao.ChatMessageRepository;
import com.example.solochat.dao.UsersRepository;
import com.example.solochat.dto.ChatRequestDto;
import com.example.solochat.model.ChatMessage;
import com.example.solochat.model.Users;
@Service
public class ChatMessageServiceImpl implements ChatMessageService {
	@Autowired
	UsersRepository usersRepository;
	@Autowired
	ChatMessageRepository chatMessageRepository;
	@Autowired
	SimpMessageSendingOperations messageTemplate;
	
	@Override
	public ResponseEntity<ChatMessage> sendSoloMessage(ChatRequestDto chatRequestDto) {
		
		
		
		
		ChatMessage chatMessage = new ChatMessage();
		Optional<Users> fromUserCheck = usersRepository.findById(chatRequestDto.getFromUsers());
		Optional<Users> toUserCheck = usersRepository.findById(chatRequestDto.getToUser());
		Users fromUser = fromUserCheck.get();
		Users toUser = toUserCheck.get();
		chatMessage.setFromUser(fromUser);
		chatMessage.setToUser(toUser);
		chatMessage.setContent(chatRequestDto.getContent());
		chatMessageRepository.save(chatMessage);
		
		String toUserIdInString = String.valueOf(chatRequestDto.getToUser());
		System.out.println(toUserIdInString);
		System.out.println(chatMessage);
		//messageTemplate.convertAndSendToUser("101","/sendMsg",chatMessage);
		messageTemplate.convertAndSend("/topic/"+toUserIdInString+"/sendMsg", chatMessage);
	
		System.out.println("below msg");
	
		
	
		
		
		return ResponseEntity.ok(chatMessage);
	}
	
	
	
}
