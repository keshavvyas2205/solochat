package com.example.solochat.services;

import org.springframework.http.ResponseEntity;

import com.example.solochat.dto.ChatRequestDto;
import com.example.solochat.model.ChatMessage;

public interface ChatMessageService {
	public ResponseEntity<ChatMessage> sendSoloMessage(ChatRequestDto chatRequestDto);	
	
}
