package com.example.solochat.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.solochat.model.ChatMessage;
import com.example.solochat.model.Users;
import com.example.solochat.requestDto.RegisterDto;
import com.example.solochat.response.DataResponse;
import com.example.solochat.response.StatusCodes;
import com.example.solochat.service.UserService;

@RequestMapping("/user")
@RestController
public class UsersController {

	@Autowired
	UserService userService;

	@PostMapping("/registeruser")
	public DataResponse registerUser(@RequestBody RegisterDto registerDto) {
		try {
			return userService.registerUser(registerDto);
		} catch (Exception e) {
			return new DataResponse(StatusCodes.ERROR, e.getMessage(), null);
		}

	}

	@GetMapping("/getuserslist/{offset}/{pageNo}")
	public DataResponse getUsersList(@PathVariable("offset") int offset, @PathVariable("pageNo") int pageSize) {
		try {
			return userService.getUsersList(offset, pageSize);
		} catch (Exception e) {
			return new DataResponse(StatusCodes.ERROR, e.getMessage(), null);
		}
	}

	@GetMapping("/getchats/{fromUserId}/{toUserId}/{offset}/{pageNo}")
	public DataResponse getChats(@PathVariable("fromUserId") long fromUserId,
			@PathVariable("toUserId") long toUserId, @PathVariable("offset") int offset,
			@PathVariable("pageNo") int pageSize) {
		try {
			return userService.getChats(fromUserId, toUserId, offset, pageSize);
		} catch (Exception e) {
			return new DataResponse(StatusCodes.ERROR, e.getMessage(), null);
		}
	}

}
