package com.example.solochat.service;

import java.util.List;

import com.example.solochat.model.ChatMessage;
import com.example.solochat.model.Users;
import com.example.solochat.requestDto.RegisterDto;
import com.example.solochat.response.DataResponse;

public interface UserService {

	DataResponse registerUser(RegisterDto registerDto);

	DataResponse getUsersList(int offset,int pageNo);

	DataResponse getChats(long fromUserId, long toUserId,int offset,int pageNo);

}
