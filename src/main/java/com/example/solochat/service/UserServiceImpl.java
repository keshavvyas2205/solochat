package com.example.solochat.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.example.solochat.dao.ChatMessageRepository;
import com.example.solochat.dao.UsersRepository;
import com.example.solochat.model.ChatMessage;
import com.example.solochat.model.Users;
import com.example.solochat.requestDto.RegisterDto;
import com.example.solochat.response.DataResponse;
import com.example.solochat.response.StatusCodes;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UsersRepository usersRepository;
	@Autowired
	ChatMessageRepository chatMessageRepository;

	@Override
	public DataResponse registerUser(RegisterDto registerDto) {
		Users u = new Users();
		u.setId(registerDto.getId());
		u.setUserName(registerDto.getUserName());
		Users res = usersRepository.save(u);
		return new DataResponse(StatusCodes.SUCCESS, "SUCCESS", res);
	}

	@Override
	public DataResponse getUsersList(int offset, int pageNo) {
		List<Users> usersList = usersRepository.findAll(PageRequest.of((offset - 1), pageNo)).toList();
		if(usersList==null)
			return new DataResponse(204, "NoUsersFound", null);
		
		DataResponse res= new DataResponse(StatusCodes.SUCCESS, "SUCCESS", usersList);


		System.out.println(res.getData());
		return res;
	}

	@Override
	public DataResponse getChats(long fromUserId, long toUserId, int offset, int pageSize) {
		Optional<Users> fromUser = usersRepository.findById(fromUserId);
		Optional<Users> toUser = usersRepository.findById(toUserId);
		System.out.println("----------------***************---------------*-*-*-*-*-*-*-*-*");
		System.out.println(fromUser.isPresent());
//		System.out.println(fromUser.isEmpty());
		System.out.println(fromUser);
		System.out.println("----------------***************---------------*-*-*-*-*-*-*-*-*");

		System.out.println(toUser.isPresent());
//		System.out.println(toUser.isEmpty());
		System.out.println(toUser);
		
		if (!fromUser.isPresent())//use this insted of null
			return new DataResponse(StatusCodes.NOT_FOUND, "SENDER NOT FOUND", null);
		if (!toUser.isPresent())
			return new DataResponse(StatusCodes.NOT_FOUND, "RECEIVER NOT FOUND", null);
		
		List<ChatMessage> chatMessagePaged = chatMessageRepository.findByFromUserAndToUserOrderByCreatedAt(
				fromUser.get(), toUser.get(), PageRequest.of((offset - 1), pageSize));
		return new DataResponse(StatusCodes.SUCCESS,"SUCCESS",chatMessagePaged);
	}
}
