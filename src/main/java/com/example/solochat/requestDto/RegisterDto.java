package com.example.solochat.requestDto;

import lombok.Data;


public class RegisterDto {
	private long id;
	private String userName;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Override
	public String toString() {
		return "RegisterDto [id=" + id + ", userName=" + userName + "]";
	}
	public RegisterDto(long id, String userName) {
		super();
		this.id = id;
		this.userName = userName;
	}
	public RegisterDto() {
		super();
	}
	
	
}
